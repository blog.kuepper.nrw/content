---
title: 'Good Mans Burger Trucks'
author: "Rüdiger Küpper"
banner: "/img/cat/essen.jpg"
categories: ["Essen"]
date: Fri, 20 Jul 2018 11:12:54 +0000
draft: false
tags: [Beef, Burger, Düsseldorf, Essen, Essen, Köln, Ratingen, Truck]
---

Ein Kollege hat uns vor ein paar Wochen einmal mit zum [Burgertruck](https://www.goodmansburgertruck.de) genommen, der ab und an in der Nähe vom Büro steht. Seit dem waren wir jedes mal Burger holen wenn der Truck da war. Super lecker und Preise sind auch noch sehr angenehm. Heute war wieder der Burger Tag und es war wieder ein Genuss. Sie sind mit 6 Trucks und in bis zu 10 Städten pro Woche.
<!--more-->

{{< postimage "Goodman's Burger Truck" "burger-n-truck.jpg" "burger-n-truck.jpg" >}}

Einen Wochenplan mit allen Adressen findest Du hier: [http://www.facebook.com/Burgertruck/](http://www.facebook.com/Burgertruck/) Lohnt sich.
