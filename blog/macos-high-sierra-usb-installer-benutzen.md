---
title: 'macOS High Sierra USB installer benutzen'
author: "Rüdiger Küpper"
banner: "/img/cat/technik.jpg"
categories: ["Technik"]
date: Fri, 09 Jun 2017 12:06:05 +0000
draft: false
tags: [Apple, Beta, Boot, High Sierra, Installation, Internet, MacOS, Partition, USB-Stick]
---

Um den USB-Installer zu benutzen, einfach den Mac booten, während dessen die Option / alt (⌥) drücken. Das Laufwerk mit dem Titel "Install macOS 10.13 Beta" auswählen.
<!--more-->

{{< postimage "" "high-sierra-install-screen.png" "high-sierra-install-screen.png" >}}

Der Computer wird dann auf die MacOS 10.13 High Sierra Beta Installation booten und die Installation kann durchgeführt werden.
