---
title: 'Pulled Pork'
author: "Rüdiger Küpper"
banner: "/img/cat/essen.jpg"
categories: ["Essen"]
date: Fri, 09 Mar 2018 20:38:05 +0000
draft: false
tags: [Alle Beiträge, BBQ, Essen, Essen, Grillen, Kochen, Ofen, Pulled Pork]
---

#### Zutaten

* 2,5 Kilo Schweinenacken ohne Knochen
* ca. 250 ml Ananassaft
* ca. 250 ml kräftige Gemüsebrühe
* 3 EL brauner Zucker
* 4 EL Senf
* 3 EL Paprikapulver, edelsüß
* 1,5 EL Salz
* 1,5 EL Puderzucker
* 1 EL Pfeffer
* 1,5 EL Kreuzkümmel
* 1,5 EL Knoblauchgranulat
* 1,5 EL Cayennepfeffer

<!--more-->

#### Zubereitung

1.  Alle Gewürze von Paprikapulver miteinander vermischen.
2.  Fleisch mit dem Senf einreiben.
3.  Anschliessend der trockenen Gewürzmischung sorgfälltig einreiben. Je sorgfältiger desto Besser wird es nachher schmecken.
4.  Das Fleisch in einen Beutel packen Luft heraussaugen oder drücken, luftdicht verschliessen und für 24 Stunden in den Kühlschrank.

{{< postimage "Pulled Pork" "img_5073-300x225.jpg" "img_5073-1024x768.jpg" >}}

Am nächsten Tag rechtzeitig das Fleisch aus dem Kühlschrank nehmen und bevor es in den Ofen kommt die Zimmertemperatur annehmen lassen.

1.  Ofen auf 110 Grad vorheizen. Ober-/Unterhitze auf keinen Fall Umluft.
2.  Ein Auflaufform oder Bräter der unter das Rost passt mit 250 ml Annanassaft, 250 ml Orangensaft ud 250 ml Brühe und den 3 EL braunen Zucker füllen.
3.  Bräter/Auflauform in den Ofen und auf dem Gitter darüber das Fleisch legen.
4.  Jetzt heisst es warten, warten, warten.
5.  Wenn das Fleisch ein Kerntempertur von 65-70 Grad hat mit dem Sud aus dem Bräter mit einem Pinsel bestreichen. Wenn nötig ruhig Annanas und Co nachschütten.

Wichtig. Immer Ruhe bewaren. Das Fleisch wird am anfang recht schnell hoch gehen. Wird dann aber Plateaus haben in denen nichts mehr passiert. Meistens bei ca. 65 Grad. Es kommt auch vor das später auch noch mal bei ca 75-78 Grad ein zweites Plateau kommt. Die Plateaus können auch schon mal bis zu 2 Stunden dauern bis es weiter geht. Also abwarten und nicht die Temperatur hoch drehen. Das wird schon.

{{< postimage "Pulled Pork" "img_5078-300x225.jpg" "img_5078-1024x768.jpg" >}}


Wenn 20-21 Stunden rum sind kann man auch schon einmal mit der [BBQ-Sosse](https://blog.pretzlaff.info/2018/03/09/bbq-sosse/) beginnen. Wer zum Pulled Pork auch [Buns](https://blog.pretzlaff.info/2018/03/09/burger-buns/) haben möchte auch diese. Beides dauert 2-3 Stunden. Aber reine Arbeitszeit ist fast gar keine. Dabei heisst es auch die meiste Zeit warten, warten, warten. Wird aber auch beides genau so belohnt wie das Pulled Pork. Unverschämt lecker. Wenn das Fleisch 95 Grad hat ist es fertig. Es kann jetzt aus dem Ofen genommen werden und auseinander gezupft werden. Es sollte jetzt aber schon von selbst auseinder fallen. Dafür nehme ich immer ein Backofenblech und lege das Fleisch drauf. Einfach mit 2 Gabeln  auseinander ziehen. Anschliessend gibt man noch etwas vom Sud und etwas von der [BBQ-Sosse](https://blog.pretzlaff.info/2018/03/09/bbq-sosse/) dazu. Fertig ist das Pulled Pork und kann gegessen werden.

{{< postimage "Pulled Pork" "img_0161-225x300.jpg" "img_0161.jpg" >}}

Beim schreiben dieses Artikels ist aufgefallen das vom fertigen Pulled Pork bis jetzt nur das Bild existiert wo die Schale schon fast leer war. Beim nächsten mal muss ich die hunrige Meute davon fern halten. Die fahren halt alle voll drauf ab.
