---
title: 'Practical Linux Security Cookbook'
author: "Rüdiger Küpper"
banner: "/img/cat/internet.jpg"
categories: ["Internet"]
date: Fri, 12 May 2017 07:53:20 +0000
draft: false
tags: [E-Book, epub, Internet, Internet, Linux, Network, Security]
---

Bei [packtpub.com](https://www.packtpub.com/packt/offers/free-learning) gibt es wieder das tägliche kostenlose E-Book. "Practical Linux Security Cookbook" heißt das heutige Buch und könnte für einige interessant sein.

<!--more-->

> As more and more organizations adopt Linux for their networks and servers, the number of security threats grows and grows. As a sysadmin, you'll be the first point of call when hackers threaten your network - and this free eBook will be your bible for building and maintaining a secure Linux system. Whether you are new to Linux administration or an experienced admin, this book will make your system secure. Walk through customizing the Linux kernel and securing local files, manage user authentication locally and remotely, patch bash vulnerability and more.
