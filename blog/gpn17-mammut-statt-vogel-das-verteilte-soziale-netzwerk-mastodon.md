---
title: 'GPN17 Mammut statt Vogel Das verteilte soziale Netzwerk Mastodon'
author: "Rüdiger Küpper"
banner: "/img/cat/internet.jpg"
categories: ["Internet"]
date: Mon, 29 May 2017 10:45:24 +0000
draft: false
tags: [dezentral, Gulaschprogrammiernacht 2017, Internet, Mastodon, Social Network]
---

Mastodon ist ein neues soziales Netzwerk, das dezentral funktioniert und OStatus implementiert (wie auch zB GNUSocial).
Hier soll erklärt werden, wie Mastodon funktioniert, welche Eigendynamik sich auf Instanzen entwickelt und Statistiken über die Nutzung der ersten Monate nach dem Abheben vorgestellt werden.
<!--more-->

Quelle: [Media.ccc.de GPN17 Mammut statt Vogel](https://media.ccc.de/v/gpn17-8575-mammut_statt_vogel)
