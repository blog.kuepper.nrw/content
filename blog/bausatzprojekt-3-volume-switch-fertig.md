---
title: 'Bausatzprojekt 3 Volume Switch fertig.'
author: "Rüdiger Küpper"
banner: "/img/cat/musik.png"
categories: ["Musik"]
date: Wed, 03 Feb 2016 00:03:32 +0000
draft: false
tags: [Alle Beiträge, Bausatz, Effektgeräte, Guitar, Musik]
---

Wie schon erwähnt ist der 3. Bausatz der erste der zusammen gebaut wurde. Der Volume Switch ist vorhin fertig geworden. Die Wahl war genau richtig. Die Zeit in denen der Lötkolben zuletzt an war ist schon etwas länger her. Da war dieser Bausatz genau richtig um wieder damit anzufangen. Zum Schluss lief es dann auch wieder richtig gut. Die zwei kleinen Fehler wurden schnell korrigiert.  Am Poti hatte ich links und rechts vertauscht und bei der LED war auch vertauscht. Kleine blöde Fehler, aber das kann ja mal passieren.
<!--more--> 
Die waren dann auch schnell gefunden und jetzt funktioniert der Volume Switch wie er soll.  Beim testen fällt auf das die Höhen wirklich bei Aktivierung konstant bleiben. Genau wie in der Beschreibung beschrieben. Leise Passagen im Song können mit dem gleichen Sound gespielt werden und es klingt nicht wie üblich dumpfer. Außerdem treffe ich bei manchen Songs am Regler der Gitarre oft nicht genau die richtige Lautstärke. Gerade wenn der Übergang sehr kurz ist.

{{< postimage "Volume Switch" "IMG_1121-1-e1454456497787-1024x768.jpg" "IMG_1121-1-e1454456497787-1024x768.jpg" >}}

{{< postimage "Volume Switch" "IMG_1122-e1454456449498-1024x768.jpg" "IMG_1122-e1454456449498-1024x768.jpg" >}}

Also fertig ist die erste Tretmine, macht Spaß beim Spielen und beim zusammen löten. Ich habe ja noch drei Stück hier liegen. Der nächste wird der Fuzz. Manche Sachen werde ich die Tage schon anfangen. Die schwierigen Sachen beim Fuzz werde ich aber erst machen wenn die 3. bzw 4. Hand da ist. Den diese netten kleinen klemmen fehlen in manchen Situationen schon. Daher habe ich bei Amazon vorhin eine bestellt. Dieses Mal ging es zwar auch ohne, aber sollte man sich lieber gleich mit bestellen. Kostet auch nur 9 €.
