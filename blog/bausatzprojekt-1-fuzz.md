---
title: 'Bausatzprojekt 1 Fuzz'
author: "Rüdiger Küpper"
banner: "/img/cat/musik.png"
categories: ["Musik"]
date: Sat, 16 Jan 2016 14:00:23 +0000
draft: false
tags: [Alle Beiträge, Bausatz, Effektgeräte, Guitar, Musik]
---

Im Frühjahr 2015 habe ich bei Mike seine ersten Effektgerätbausätze gesehen. Recht günstig diese kleinen Dinger. Platine, Schalter, Buchsen und weitere Kleinteile für kleines Geld. Dazu kann man direkt die passenden Gehäuse bestellen. Auch gleich mit vorgebohrten Löchern wenn man nicht selbst bohren möchte. Blank oder farbig. Je nachdem wie man es möchte. Also ist der Selbstgestaltung keine Grenze gesetzt. Mike war von Fuzz sehr begeistert. Bei einer Session letzen Sommer meinte er:
<!--more-->

> Ich habe mich in den Fuzz verliebt.

Er hörte sich auch sehr gut an. Also nicht Mike, der Fuzz. Auf der Internetseite von [Musikding.de](http://www.musikding.de) sind auch Soundbespiele bei den Bausätzen zu hören.

{{< postimage "Fuzzy" "fuzzy-300x240.jpg" "fuzzy.jpg" >}}


[Der Bender Mk3 - Germanium Fuzz Bausatz](http://www.musikding.de/Der-Bender-Mk3-Germanium-Fuzz-Bausatz) 26,- € + Gehäuse insgesamt 38,50 €. Ich habe mich für das vorgebohrte Gehäuse entschieden. Zusätzlich habe ich noch 5 rote Regler bestellt. Die restlichen werde ich für den zweiten Bausatz benutzen. Da ich später noch weitere Bausätze bestellen möchte kann ich auch später noch die Farben variieren. Später werde ich noch einen Artikel zum Compressor schreiben. Wenn beide dann da sind gehe ich noch etwas mehr auf Details zu den Bausätzen ein. Dann auch mit mehr Bildern von der Lieferung, auspacken und vom basteln.
