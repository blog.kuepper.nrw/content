---
title: "WiFi Hotspot und Hugo Grid Theme"
author: "Rüdiger Küpper"
date: "Sat, 18 Aug 2019 22:30:00 GMT"
draft: false
banner: "/img/posts/2019/08/19/wifi.png"
categories: ["Internet"]
tags: [Router,Hotspot,Chillispot,Hugo,CMS,Theme,Grid,Responsive]
---

# EdgeRouterX und Chillispot Captive Portal

Auf dem EdgeRouterX [^1] habe ich zum testen die Tage auch einmal [Chillispot Captive Portal](http://www.chillispot.org) installiert.

Auf dem WiFi AccessPoint eine neue SSID mit neuem VLAN angelegt, SwitchPort getagged und auf dem EdgeRouterX das VLAN angelegt mit den passenden Config für Chillispot.
<!--more-->
Für die Authentifizierung habe ich [Hotspotsystem.com](https://www.hotspotsystem.com) genommen. Die Einrichtung und das aktivieren ist recht simple. Und wenn man neben dem Gast WiFi noch ein Pay-WiFi anbieten möchte kann man das damit sehr gut umsetzen.

# Hugo CMS Grid Theme

Zwischen dem ganzen Netzwerkkram [^2], [^3], [^4], [^5] die letzten Tage habe ich mich mal an ein eigenes Theme für dieses Blog gemacht. Das ist nicht so mein Ding. Nicht so wie Netzwerk und Serverkram.

Aber so ein HTML5 Grid werde ich ja wohl hinbekommen. Hauptsächlich sollte es immer auf mobilen Geräten immer passen. Keine Bilder die zu groß sind und Dinge, wie Sidebar sollen einfach wo anders hin verschwinden wo sie erst einmal nicht stören. Oder manche Sachen in der mobilen Ansicht einfach komplett verschwinden.

Was auf mobile passt, das passt auch auf dem Desktop. Also Mobile-First.

An ein paar Ecken muss ich noch ran. Aber für einen Tag HTML- und CSS-Session schon mal nicht schlecht. Jetzt noch mal in Ruhe etwas aufräumen und dann weiter verbessern und verschönern.

[^1]: EdgeRouterX [Ubiquiti](https://www.ui.com/edgemax/edgerouter-x-sfp/)
[^2]: [Netzwerk Umbaulobar](https://blog.kuepper.nrw/2019/08/09/netzwerk-umbaulabor/)
[^3]: [Netzwerkumbau VLANs und Firewall](https://blog.kuepper.nrw/2019/08/09/netzwerkumbau-vlans-und-firewall/)
[^4]: [VLANs und Firewall](https://blog.kuepper.nrw/2019/08/12/vlans-und-firewall/)
[^5]: [Networkserver of Death](https://blog.kuepper.nrw/2019/08/18/networkserver-of-death/)
