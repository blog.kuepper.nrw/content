---
title: 'Bastelprojekt 1 Bender Mk3 Fuzz fast fertig.'
author: "Rüdiger Küpper"
banner: "/img/cat/musik.png"
categories: ["Musik"]
date: Wed, 10 Feb 2016 21:28:27 +0000
draft: false
tags: [Alle Beiträge, Bausatz, Effektgeräte, Guitar, Musik]
---

Nachdem der Volume Switch letzte Woche fertig zusammmen gebaut hatte wurde gleich als erstes die 3. Hand bestellt. Bei dem Effektgerät war es ohne kein Problem. Aber das nächste hat eine Platine, auf die ein paar Widerstände, Kondensatoren usw draufgelötet werden wollen. Trotzdem wollte ich letzte Woche auch schon einmal mit dem Fuzz anfangen. Die Potis, Klinkestecker, LED und Schalter schon mal verdrahten ist als Entspannung neben dem TV auch etwas für Grobmotoriker. Da geht nichts bei schief. Schief ging er später etwas. Aber nur eine Kleinigkeit. Als ich sehr schnell mit der Verkabelung der oben genannten Teile fertig war und die Platine in der Hand halte geht mir der Gedanke durch den Kopf:
<!--more-->

> Ach komm schon. Die Wiederstände sind kein Problem.

Gedacht getan und schon waren sie auf der Platine verlötet. Das gleiche spielte sich dann nach einander mit Kondensatoren, Transostoren, Dioden und allem anderen ab.

> Ach komm schon. Die ... sind kein Problem.

{{< postimage "Bender MK3 FUZZ" "IMG_1435-e1455138449549-1024x768.jpg" "IMG_1435-e1455138449549-1024x768.jpg">}}

Bis zur vorletzen Diode hat auch alles sehr gut geklappt. Ok, mit der 3. Hand wären manche Löststellen besser geworden. Die kam aber erst am nächsten Tag an. Die vorletzte Diode war eine A1108. Wie bei allen anderen Teilen wollte ich die Kontakte zurecht biegen um sie passend auf die Platine zu bekommen. Da machte es dann einmal knack und die war hin. Dafür klappte dann aber die letzte besser.

{{< postimage "Bender MK3 FUZZ 2" "IMG_7208-e1455138417739-1024x768.jpg" "IMG_7208-e1455138417739-1024x768.jpg" >}}

Direkt einmal 5 Stück als Ersatz bestellt. Sicher ist sicher. Die sind auch die Tage angekommen. Am Samstag werde ich dann die letzte Diode einbauen und sehen ob alles andere richtig zusammen gebaut wurde. Hier noch kurz die Info zur 3. Hand von Amazon.

<!--
\[AMAZONPRODUCTS asin="B000T9UCHI" features="1" desc="1" locale="de" public\_key="AKIAJDRNJ6O997HKGXW" private\_key="Nzg499eVysc5yjcZwrIV3bhDti/OGyRHEYOWO005" partner\_id="blogpretzlaff-21"\]
-->
