---
title: 'Bausatzprojekt 4 Bluelay - Delay'
author: "Rüdiger Küpper"
banner: "/img/cat/musik.png"
categories: ["Musik"]
date: Thu, 21 Jan 2016 15:00:38 +0000
draft: false
tags: [Alle Beiträge, Bausatz, Effektgeräte, Guitar, Musik]
---

Die letzte Tretmiene ist ein Delay. Das Bluelay ist ein digitales Delay, welches laut Beschreibung analog klingen soll. Auf der Artikelseite sind zwei Soundbeispiele. Genau was ich gesucht habe und mir zusagt.

{{< postimage "Das Bluelay Delay" "img_1075.jpg" "img_1075.jpg" >}}

Auch hier der Preis: 22,50 € + Gehäuse 11,50 €. Das war dann der letzte Bausatz der neuen Effektgeräte. Jetzt fängt der Bastelspass bald an. Die nächsten Tage werden alle Schaltpläne ausgedruckt und erst einmal das Bastelprojekt 3 (Volume Schalter) gemacht, oder auch nicht. Die beiden zuletzt bestellten sind ja noch gar nicht da. Also doch am Wochenende eine der anderen Tretminen. Das ist von den vier Bausätzen der leichteste. Genau das richtige um nach so langer Zeit langsam mit dem Löten anzufangen.
