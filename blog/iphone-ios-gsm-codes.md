---
title: 'iPhone iOS GSM Codes'
author: "Rüdiger Küpper"
banner: "/img/cat/technik.jpg"
categories: ["Technik"]
date: Tue, 02 Feb 2016 15:49:36 +0000
draft: true
tags: [Alle Beiträge, Codes, GSM, iOS, iPhone, Mailbox, Rufumleitung, Technik, Telefon, Weiterleitung]
---

\[table id=1 /\] Philip Tusch von Apfelpage erwähnt in [einem Artikel](http://www.apfelpage.de/2016/02/04/ueberbleibsel-mit-funktionen-67-nuetzliche-gsm-codes-fuer-das-iphone/ ) noch das hier der Feldtestmodus erwähnt werden könnte: "Ergänzen könnte man die Liste noch um den Code für den Feldtestmodus, der beispielsweise genau in Dezibel anzeigt, wie stark euer Mobilfunkempfang aktuell wirklich ist: _3001#12345#_. Diese Funktion gibt es übrigens auch schon seit 2007."
