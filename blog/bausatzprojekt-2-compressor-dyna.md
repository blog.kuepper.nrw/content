---
title: 'Bausatzprojekt 2 Compressor-Dyna'
author: "Rüdiger Küpper"
banner: "/img/cat/musik.png"
categories: ["Musik"]
date: Tue, 19 Jan 2016 12:10:43 +0000
draft: false
tags: [Alle Beiträge, Bausatz, Effektgeräte, Guitar, Musik]
---

Das 2. Bausatzprojekt wird ein Compressor. [Der Compressor Dyna - Compressor Bausatz](http://www.musikding.de/Der-Compressor-Dyna-Compressor-Bausatz) + Gehäuse kostet der Bausatz bei [musikding.de](http://www.musikding.de) 35,- €
<!--more-->

{{< postimage "Der Compressor Dyna Compressor Bausatz" "Der-Compressor-Dyna-Compressor-Bausatz.jpg-300x300.png" "Der-Compressor-Dyna-Compressor-Bausatz.jpg.png" >}}


Der Compressor Dyna Compressor Bausatz (Bildquelle: Musikding.de)

Der Compressor und der Fuzz wurden heute geliefert. Dazu später mehr, Bilder und noch ein paar Worte zum Versender.
