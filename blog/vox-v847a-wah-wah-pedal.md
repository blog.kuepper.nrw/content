---
title: 'VOX V847A Wah Wah Pedal'
author: "Rüdiger Küpper"
banner: "/img/cat/musik.png"
categories: ["Musik"]
date: Fri, 12 Feb 2016 10:00:38 +0000
draft: false
tags: [Alle Beiträge, Effektgeräte, Guitar, Musik, Test, VOX, Wah-Wah, Zubehör]
---


Dieses mal nicht selbst gelötet, dafür aber selbst gekauft. Da ich noch kein Wah-Wah habe, aber in Zukunft eins brauche, musste ein Wah-Wah her. Irgendwie stehen Effektgerätehersteller alle aktuell auf rot. Der Karton von VOX ist genau so rot wie der von dem Compressor den ich letztens getestet habe. Sieht nicht schlecht aus, wirkt stylisch. Wird aber trotzdem im schrank landen, denn das VOX Wah V847A wird mit einer Tasche geliefert.
<!--more-->
{{< postimage "VOX V847A Wah Wah Pedal" "IMG_1127-e1455234520305-150x150.jpg" "IMG_1127-e1455234520305-768x1024.jpg" >}}

{{< postimage "VOX V847A Wah Wah Pedal" "IMG_1128-e1455234629727-150x150.jpg" "IMG_1128-e1455234629727-768x1024.jpg" >}}

Die sehr wahr scheinlich im gleichen Schrank landen wird wie der Karton. Außer das Pedal passt nachher mal nicht mehr auf das Pedalboard. Vom Sound her ist es ähnlich wie das Cry-Baby von VOX. Man muss aber schon genau hinhören um wirklich einen Unterschied feststellen zu können. Beim "nur mal beide testen" ist es schwer diesen Unterschied wirklich zu hören.

{{< postimage "VOX V847A Wah Wah Pedal" "IMG_1129-e1455234758942-150x150.jpg" "IMG_1129-e1455234758942-768x1024.jpg" >}}

Gitarristen die ein Cry-Baby regelmässig spielen werden es sehr wahrscheinlich hören. Was am Anfang sehr gewöhnungsbedürftig war, ist der starke Druckpunkt um es zu aktivieren. Man muss das Pedal schon sehr feste nach vorne kippen. Im sitzen sehr schwer, aber im stehen geht es sehr gut. Ein brauchbares Wah-Wah Pedal für eine sehr guten Preis.

{{< postimage "VOX V847A Wah Wah Pedal" "IMG_1131-e1455234812127-150x150.jpg" "IMG_1131-e1455234812127-1024x768.jpg"  >}}

Vom Sound her nicht zu extrem und eher ein weichers Wah-Wah. Eben der Klassiker unter den Wah-Pedalen.

<!-- Unten im Player sind 4 Soundbeispiele mit dem VOX V847A. Zwei davon Clean und zwei verzerrte.     \[playlist artists="false" ids="518,517,516,515"\] -->
