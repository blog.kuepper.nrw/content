---
title: 'Lieferung Fuzz und Compressor'
author: "Rüdiger Küpper"
banner: "/img/cat/musik.png"
categories: ["Musik"]
date: Tue, 19 Jan 2016 15:09:31 +0000
draft: false
tags: [Alle Beiträge, Bausatz, Effektgeräte, Guitar, Musik]
---

Die ersten beiden Bausätze sind eingetroffen. Der Compressor-Dyna und der "Der Bender Mk3 Fuzz". Bei der Bestellung hatte ich meinen Namen mit angegen und den Firmenname. Da wir uns auch privates Zeug in die Firma schicken lassen dürfen ging auch dieses Päckle an unsere Poststelle. Auf dem Etikett stand nur der Firmenname mit der Adresse. Der Versender [Musikding.de](http://musikding.de) hat aber das fehlen des Empfängernamen bemerkt und handschriftlich noch mit drauf gepackt.
<!--more-->


{{< postimage "Der Compressor Dyna Compressor Bausatz" "img_1062-300x225.jpg" "img_1062-1024x768.jpg">}}
{{< postimage "Compressor-Dyna" "Compressor-Dyna-300x225.jpg" "Compressor-Dyna-1024x768.jpg" >}}


Das hatte ich bei anderen schon mal anders erlebt und Pakete konnten nicht zugeordnet werden und mussten wieder zurück an den Absender gehen. Lob an den Absender.
In dem Packet lagen die beiden Bausätze, 2 Gehäuse und ein kleines Tütchen mit den Reglern.

Als erstes werde ich den Fuzz zusammen bauen. Nachher als erstes den Lötkolben bereitlegen und anfangen.
