---
title: 'DSL Traffic 2017'
author: "Rüdiger Küpper"
banner: "/img/cat/technik.jpg"
categories: ["Technik"]
date: Tue, 02 Jan 2018 13:03:29 +0000
draft: false
tags: [Internet]
---

Unser DSL-Traffik 2017 Angabe in Megabytes

Monat   | Gesendet |   Empfangen | Gesamt
--------|----------|-------------|-------
Jan	    | 4124	   | 16815	     | 20939
Feb	    | 1078	   | 403	       | 1481
Mär	    | 1199	   | 446	       | 1645
Apr	    | 2464	   | 36476	     | 38940
Mai	    | 92979	   | 615268	     | 708247
Jun	    | 138402	 | 664743	     | 803145
Jul	    | 116406	 | 507155	     | 623561
Aug	    | 35654	   | 471810	     | 507464
Sep	    | 31362	   | 428400	     | 459762
Okt	    | 24072	   | 549927	     | 573999
Nov	    | 44095	   | 914362	     | 958457
Dez	    | 57889	   | 1141699     | 1199588
Gesamt:	| 549724   | 5347504     | 5897228
<!--more-->

Anfang des Jahres war der Traffik hauptsächlich über den anderen Anschluss und taucht hier in der Statistik nicht auf.
Im März und April war noch Renovierungszeit und es wurde weniger gestreamt. Wir haben uns nach der Arbeit eher selbst
nur noch in Bett gestreamt. Im Oktober wurde das 1. OG mit aufgeklemmt und gepatched. Daher dann ab Ende Oktober der
Anstieg des Traffiks.

In den nächsten Tagen kommt noch das EG mit dazu, dann ist das Netzwerk endlich komplett. Im 2. und 3. OG ist schon
alles fertig. 2. und 3. OG haben jeweils 2 x 16 Port Switche. Beide Etagen sind mit 4 Gigabit (LAG) verbunden.
Im 2. OG ist dann noch der AccessPoint und ein Cisco 10 Port für Layer 3, der die unteren Etagen mit anbindet.
Das 1. OG ist über den Cisco Switch mit 2 Leitungen (LAG) zum dort verbauten Cisco Switch verbunden.
Das gleiche wird dann bald auch noch jeweils aus dem 1. und 2. OG in das EG gemacht. So das 4 Leitungen (LAG) 4 Gigabit
theoretisch im ganzen Haus können.

Wenn das gemacht ist werden alle Geräte über 2 Leitungen ins Internet gehen und die Jahresstatistik nächstes Jahr wird beide Anschlüsse beinhalten. Aber hier jetzt erst einmal die Graphen von 2017 mit nur einem Anschluss.
Ein- und ausgehender Traffic:

{{< postimage "" "inoutstacked.svg" "inoutstacked.svg" >}}

Ein- und ausgehender Traffic (Summe)
{{< postimage "" "sum.svg" "sum.svg" >}}

Ein- und ausgehender Traffic:
{{< postimage "" "inout.svg" "inout.svg" >}}

Ausgehender Traffic:
{{< postimage "" "outgoing.svg" "outgoing.svg" >}}

Eingehender Traffic:
{{< postimage "" "incomming.svg" "incomming.svg" >}}


Danke an Dennis für [den Hinweis](https://instant-thinking.de/2018/01/02/2017-in-vdsl-zahlen/) das ein Jahr wieder rum ist.
