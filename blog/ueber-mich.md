---
title: 'Über mich'
author: "Rüdiger Küpper"
banner: "/img/cat/technik.jpg"
categories: ["Technik"]
date: Fri, 08 Jan 2016 01:16:15 +0000
draft: true
---

Irgendwie muss man manchmal doch was raushauen. Daher werde ich jetzt wieder öfters bloggen. Und ja, es ist mit Wordpress. Eigentlich wollte ich auch für das Blog mein Content Ninja benutzen. Da ich aber beim bauen des Content Ninja CMS den Blog Teil raus geworfen habe, um erst einmal das CMS sauber hinzubekommen, sich in der Zeit aber einiges geändert hat, wird Integration erst einmal etwas Zeit brauchen. Der Code vom Content Ninja Blog soll genau so schnell werden wie beim Content Ninja CMS. Beides war mal ein Stück Software. Sie werden aber erst einmal zwei eigenständige Projekte bleiben. Ich werde mir aber mal überlegen wie ich sie sauber zusammen führen kann. Ach ja, die alten Inhalte. Die sind noch schön brav im Archiv vergraben. Teilweise werde ich ein paar davon bestimmt noch einmal ausgraben und hier mit dem alten Datum versenken. Aber nicht alle.

<!--more-->
