---
title: 'Bausatzprojekt 3 Volume Schalter'
author: "Rüdiger Küpper"
banner: "/img/cat/musik.png"
categories: ["Musik"]
date: Thu, 21 Jan 2016 15:00:38 +0000
draft: false
tags: [Alle Beiträge, Bausatz, Effektgeräte, Guitar, Musik]
---

Die ersten beiden Bausätze sind gerade da, da habe ich die nächsten zwei bestellt. Da ich mir ein nettes Board zusammenstellen möchte habe ich noch einmal durch den Shop gestöbert und eine Volume Schalter, so wie den Bluelay - Delay Bausatz.  
<!--more-->

{{< postimage "Volume Schalter Bausatz" "img_1074.jpg" "img_1074.jpg" >}}

Den Volume Schalter habe ich aus zwei Gründen gewählt.

1.  Bei Solos mit leisem Part kann ich gezielt an diesen Stellen auf eine bestimmte Lautstärke per Fußschalter umschalten.
2.  Anders herum möchte ich meine gewünschte Lautstärke haben und bei Bedarf schnell noch etwas drauf drehen. Wenn die anderen mal lauter wurden oder meine Einstellung an manchen Stellen nicht laut genug ist schnell noch eine Schüppe drauf legen zu können.

Das mache ich aktuell direkt am Volumeregler an der Gitarre. Bei manchen Songs muss das sehr schnell gehen, da die Übergänge sehr kurz sind. Dabei genau die richtige Lautstärke zu treffen geht auch mal schief. Mal ist man zu laut oder mal zu leise. Einen weiteren Vorteil bei diesen Volume Schalter, gegen über dem Regler an der Gitarre, ist der parallel geschaltete Kondensator, durch dem zu keinem  Höhenklau kommt. Der Bausatz kostet 8,50 € + 12,00 € für das Gehäuse.
