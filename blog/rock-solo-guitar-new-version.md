---
title: 'Rock - Solo Guitar (New Version)'
author: "Rüdiger Küpper"
banner: "/img/cat/musik.png"
categories: ["Musik"]
date: Thu, 14 Jan 2016 23:00:56 +0000
draft: false
tags: [Alle Beiträge, Guitar, Musik, Solo, Song, Track]
---


{{< soundcloud 235962771 "Rock Solo Guitar - New Version" >}}
<!--more-->
https://soundcloud.com/rpr-3/rock-solo-guitar-new-version
