---
title: 'Textcraft Pocket aktuell kostenlos'
author: "Rüdiger Küpper"
banner: "/img/cat/organisation.jpg"
categories: ["Organisation"]
date: Wed, 16 Aug 2017 09:40:26 +0000
draft: false
tags: [Apple, Application, Editor, iOS, Markdown, Organisation, pdf, Text, Textverarbeitung]
---

Lesen, schreiben, korrigieren, recherchieren und teilen! [Textkraft Pocket](https://itunes.apple.com/de/app/textkraft-pocket/id909848112?mt=8) für iPhone und Apple Watch ist eine professionelle Schreib-App und komfortabler Dokumenten-Reader.

Das Besondere an Textkraft ist, dass du alle Dokumente für dein Projekt in einer App hast, Seite an Seite, ohne Ladezeit zugriffsbereit. Mit einer Wischgeste oder der Dokumentenübersicht wechselst du vor und zurück durch Texte, Notizen, Office-Dokumente, PDF-Dateien und andere Vorlagen, die du für deine Textarbeit benötigst
<!--more-->

Erweiterte Offline-Wörterbücher mit Synonymen und viele integrierte Online-Nachschlagewerke helfen dir bei Rechtschreibung, Sprache, Ausdruck, Grammatik, Definition und Übersetzung. Präzise Cursornavigation, schnelle Textauswahl und viele zusätzliche Tastaturfunktionen optimieren das Schreiben.


[![textcraft_pocket_screen](/hugo/img/posts/textcraft_pocket_screen.jpg)](/hugo/img/posts/textcraft_pocket_screen.jpg)
