---
title: 'Mastodon: social.pretzlaff.co'
author: "Rüdiger Küpper"
banner: "/img/cat/internet.jpg"
categories: ["Internet"]
date: Wed, 14 Jun 2017 12:25:34 +0000
draft: false
tags: [dezentral, Facebook, free, Internet, Mastodon, OpenSource, Social Network, Twitter]
---

Auf [social.pretzlaff.co](https://social.pretzlaff.co) läuft seit ein paar Tagen eine Mastodon Instanz. Mein User ist dort: [rpr](https://social.pretzlaff.co/@rpr). Registration ist dort aktiviert. Alternativ kann man sich auch einen Account auf einer [der vielen anderen Mastodon Instanzen](https://instances.mastodon.xyz/list) anlegen. Die Kommunikation funktioniert ja schließlich zwischen den Instanzen und ist ja auch der Sinn von Mastodon und dem dezentralem Aufbau. Quelle: [Wikipedia](https://de.wikipedia.org/wiki/Mastodon_(Software))

<!--more-->

> Mastodon ist ein verteilter Mikrobloggingdienst, der 2016 von Eugen Rochko entwickelt wurde; einem deutschen Programmierer aus Jena. Im Gegensatz zu großen Plattformen wie Twitter ist Mastodon als dezentrales Netzwerk konzipiert. Benutzer können einer beliebigen Instanz beitreten oder selbst eine eigene betreiben. Mastodon ist kompatibel zu GNU Social sowie allen OStatus-Diensten. Das Projekt ist Freie Software und steht mit seinem Quelltext unter der GNU Affero General Public License zur Verfügung. Entwicklung und Betrieb der Instanz mastodon.social werden durch Spenden finanziert. Auf Mastodon können angemeldete Nutzer telegrammartige Kurznachrichten verbreiten. Diese Nachrichten werden „Toots“ oder übersetzt „Tröts“ genannt. Mastodon erlaubt pro Post (Toot) 500 Zeichen.
