---
title: 'Nightly Session Part 1 Blues In A Minor'
author: "Rüdiger Küpper"
banner: "/img/cat/musik.png"
categories: ["Musik"]
date: Fri, 15 Jan 2016 00:38:56 +0000
draft: false
tags: [Alle Beiträge, Guitar, Musik, Solo, Song, Track]
---

{{< soundcloud 242004759 >}}

<!--more-->
https://soundcloud.com/rpr-3/nightly-session-part-1-blues-in-a-minor
