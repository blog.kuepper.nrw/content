---
title: 'Nightly Session Part 2 Rock Power Ballade'
author: "Rüdiger Küpper"
banner: "/img/cat/musik.png"
categories: ["Musik"]
date: Fri, 15 Jan 2016 00:39:43 +0000
draft: false
tags: [Alle Beiträge, Guitar, Musik, Solo, Song, Track]
---


{{< soundcloud 242004962 >}}
<!--more-->
https://soundcloud.com/rpr-3/nightly-session-part-2-rock-power-ballade
