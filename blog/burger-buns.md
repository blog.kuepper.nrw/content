---
title: 'Burger Buns'
author: "Rüdiger Küpper"
banner: "/img/cat/essen.jpg"
categories: ["Essen"]
date: Fri, 09 Mar 2018 21:35:13 +0000
draft: false
tags: [Alle Beiträge, Backen, Burger Buns, Essen, Essen, Hamburgerbrötchen, Ofen]
---

Zutaten für Hamburgerbrötchen / Burger Buns

*   200 ml warmes Wasser
*   4 EL Milch
*   1 Würfel Hefe
*   35 g Zucker
*   8 g Salz
*   80 g Butter
*   500 g Mehl Typ 550
*   1 Ei

<!--more-->

{{< postimage "Burger Buns" "img_0150.jpg" "img_0150.jpg" >}}

Wasser und Zucker in eine Schüssel geben. Die Hefe zerbröselt dazugeben. Alles verrühren und für 5 Minuten ruhen lassen. Jetzt alle restlichen Zutaten dazu geben, gut vermischen und durchkneten. Kann auch in der Maschine gemacht werden. Nach ca. 5 Minuten sollte es einen geschmeidigen Teig.

{{< postimage "Burger Buns" "img_0154.jpg" "img_0154.jpg" >}}

Den Teig für eine Stunde an einem warmen Ort ruhen lassen. In dieser Zeit mache ich immer den "Klebstoff" für den Sesam. Dieser sollte Zimmertemperatur haben, da sonst die Buns beim bepinseln zusammen fallen könnten.

*   1 Ei
*   2 EL Milch
*   2 EL Wasser
*   Sesam

Ei, Wasser und Milch in einer kleinen Schale gut mit dem Schneebesen vermengen. Abdecken und beiseite stellen bis kurz bevor die Buns in den Ofen können. Nach einer Stunde ist der Teig gut aufgegangen und kann weiter verarbeitet werden.

{{< postimage "Burger Buns" "img_0156.jpg" "img_0156.jpg" >}}

Man nimmt jetzt ca 80-90 Gramm und formt den Teig in der Hand zu einer Kugel. Dabei ruhig ordentlich arbeiten. Das lässt die Buns nachher perfekt aussehen. Die Kugel auf das Blech legen und leicht herunter drücken. Aus dem Teig bekommt man zwischen 10 und 12 Buns.

{{< postimage "Burger Buns" "img_0157-1.jpg" "img_0157-1.jpg" >}}

Da diese Buns nicht für Burger gewesen sind, sondern für das [Pulled Pork](https://blog.pretzlaff.info/2018/03/09/pulled-pork/) sind sie nicht alle gleich gross und nicht perfekt auf 10 cm Durchmesser getrimmt. Alles darf jetzt wieder eine Stunde ruhen. Und werden jetzt wieder ein wenig wachsen. Daher nicht zu sparsam mit dem Platz sein. Später im Ofen werden sie dann auch noch grösser. Daher nicht zu eng auf das Blech legen. Ist die Stunde rum nimmt man das Ei-, Wasser- und Milchgemisch und verteilt mit einem Pinsel es auf die Buns. Jetzt noch Sesam auf die Buns und ab in den Ofen damit. Der Ofen wird auf 200 Grad Ober-/Unterhitze vorgeheizt. Die Buns bleiben für 15-20 Minuten im Ofen. Sie sollten oben Goldbraun sein.

{{< postimage "Burger Buns" "img_0159.jpg" "img_0159.jpg" >}}

Die fertigen Buns sind super lecker und schön fluffig.
