---
title: 'BBQ-Soße'
author: "Rüdiger Küpper"
banner: "/img/cat/essen.jpg"
categories: ["Essen"]
date: Fri, 09 Mar 2018 22:01:19 +0000
draft: false
tags: [Alle Beiträge, BBQ, Essen, Essen, Grillen, Kochen, Sosse]
---

Leckere BBQ-Soße einfach selbst gemacht.

*   1 Knoblauchzehe
*   50 ml Apfelessig
*   200 ml Apfelsaft
*   200 g Tomatenketchup
*   1 EL Sojasauce
*   1 TL Worcestersauce
*   4 EL Ahornsirup
*   1-2 TL geräuchertes Paprikapulver
*   1 EL Whiskey (Ich nehme immer gerne einen rauchigen/torfigen Whiskey)
*   Salz, Pfeffer

<!--more-->

Alle Zutaten ausser den Ahornsirup in einen Topf und 20 Minuten leicht köcheln lassen. Anschließend den Ahornsirup dazugeben und abschmecken. Das war es eigentlich schon. Jetzt muss sie Soße einfach nur noch weiter auf kleiner Stufe offen kochen. Keinen Deckel auf den Topf, da sonst das kondensierte Wasser wieder in die Soße tropft. Die Soße soll jetzt reduzieren und eine zähflüssige BBQ-Soße werden.
