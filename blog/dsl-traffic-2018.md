---
title: 'DSL Traffic 2018'
author: "Rüdiger Küpper"
banner: "/img/cat/technik.jpg"
categories: ["Technik"]
date: Fri, 04 Jan 2019 10:51:35 +0000
draft: fasle
tags: [2018, DSL, Internet, Traffik]
---

Unser DSL-Traffik 2018 Angabe in Terabytes

Monat   | Gesendet   |   Empfangen | Gesamt
--------|----------|-----------|-------
Jan	    | 0.07	   | 1.20	     | 1.30
Feb	    | 0.07	   | 0.95	     | 1.05
Mär	    | 0.73	   | 0.92	     | 1.02
Apr	    | 0.06	   | 0.81	     | 0.87
Mai	    | 0.08	   | 1.28	     | 1.36
Jun	    | 0.07	   | 0.71	     | 0.78
Jul	    | 0.10	   | 0.82	     | 0.92
Aug	    | 0.06	   | 1.01	     | 1.07
Sep	    | 0.08	   | 1.09	     | 1.18
Okt	    | 0.86	   | 1.01	     | 1.10
Nov	    | 0.04	   | 1.17	     | 1.21
Dez	    | 0.04	   | 1.11	     | 1.16
Gesamt:	| 2.26     | 12.08     | 13.02

<!--more-->
{{< postimage "DSL Traffic IN/OUT/Summe" "traffic_2018.png" "traffic_2018.png" >}}

Wie auch schon im [letzten Jahr](https://blog.kuepper.nrw/2018/01/02/dsl-traffic-2017/) auch dieses Jahr wieder die DSL Traffik Statistik.

Der Dennis hat seine [auch schon online](https://instant-thinking.de/2019/01/02/2018-in-vdsl-zahlen/).

Und [Michel](https://pixelscheucher.de/vdsl-nutzung-2018/) so wie so. Die Grafiken dazu werden später noch nachgereicht.
