---
title: 'Boris Brejcha Musik fürs Büro'
author: "Rüdiger Küpper"
banner: "/img/cat/musik.png"
categories: ["Musik"]
date: Thu, 19 Jul 2018 16:32:23 +0000
draft: false
tags: [BORIS BREJCHA, High-Tech Minimal, Mix, Mixed, Musik, Track]
---

[Carsten](https://blog.zn80.net/) hat heute eine Track von [Boris Brejcha](http://www.borisbrejcha.de) [im Blog gepostet](https://blog.zn80.net/2018/07/19/boris-brejcha-au-main-square-festival-2018/). Und ja, der ist schon was ganz feines während der Arbeit. Der Track und andere von ihm werden jetzt öfters laufen.
<!--more-->
{{< youtube juhEm_J1r70 >}}
