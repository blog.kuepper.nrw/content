---
title: "Recent Posts Widget"
author: "Rüdiger Küpper"
# authorpage: "ruedigerkuepper"
date: "Mon, 05 Aug 2019 22:00:00 +0200"
draft: false
banner: "/img/posts/2019/08/05/programming.png"
categories: ["Technik"]
tags: [Hugo,CMS,Sidebar,Widgets]
---

### Recent Posts Widget für die Sidebar

In meinen Blogs hatte ich immer ein Widgets für die letzte Posts. Das habe ich gerade auch für das Hugo CMS erstellt.

Im Theme unter Layouts -> Partials -> Widgets habe ich eine Datei `lastposts.html` erstellt:
<!--more-->
{{< highlight html "linenos=table,hl_lines=1 9-12 16,linenostart=1" >}}
{{ if .Site.Params.widgets.recent_posts }}
  <div class="panel panel-default sidebar-menu">
    <div class="panel-heading">
      <h3 class="panel-title">Letzte Artikel</h3>
    </div>

    <div class="panel-body">
      <ul class="nav nav-pills nav-stacked">
      {{ $count := .Site.Params.widgets.recent_posts }}
      {{ range first $count .Pages }}
        <li><a href="{{ .Permalink }}">{{ .Name }}</a></li>
      {{ end }}
      </ul>
    </div>
  </div>
{{ end }}
{{< / highlight >}}

Die Zeilen 1, 9-12 und 16 sind markiert. Das sind Zeilen die das Widget steuern bzw. die Anzahl an konfigurierten letzten Posts ausgeben.

* Zeile 1 überprüft ob das Widget aktiv ist.
* Zeile 9 setzt die Variable $count auf die konfiguriere Anzahl der posts
* Zeile 10 bis 12 durchläuft die letzte $count Posts und zeigt sie an.

Das funktioniert auf der Startseite gut, aber in den Artikeln bleibt die Liste leer.

Also wurde jetzt noch folgender Code ersetzt:
{{< highlight html "linenos=table,hl_lines=1 2,linenostart=1" >}}
{{ $count := .Site.Params.widgets.recent_posts }}
{{ range first $count .Pages }}
  <li><a href="{{ .Permalink }}">{{ .Name }}</a></li>
{{ end }}
{{< / highlight >}}

Durch:
{{< highlight html "linenos=table,hl_lines=1 2,linenostart=1" >}}
{{ $pages := where .Site.RegularPages "Type" "in" .Site.Params.mainSections }}
{{ range first $count $pages }}
<li><a href="{{ .Permalink }}">{{ .Name }}</a></li>
{{ end }}
{{< / highlight >}}

Jetzt wird das Widget auf allen Seiten befüllt.

### Konfiguration des Widget

Das Widget kann in der `config.toml` konfiguriert werden.

{{< highlight toml "linenos=table,hl_lines=7,linenostart=157" >}}
...
# Enable and disable widgets for the right sidebar
[params.widgets]
    categories = true
    tags = true
    search = true
    recent_posts = 10
...
{{< / highlight >}}

Das Widget kann mit `false` deaktiviert werden.
Die Anzahl kann frei gewählt werden.
