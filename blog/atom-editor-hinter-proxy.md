---
title: 'Atom Editor hinter Proxy'
author: "Rüdiger Küpper"
banner: "/img/cat/internet.jpg"
categories: ["Internet"]
date: Mon, 18 Sep 2017 16:35:46 +0000
draft: false
tags: [free, Installation, Internet, Internet, Linux, MacOS, programming]
---

Da will man sich im [Atom Editor](https://atom.io) mal eben das Package "script" installieren, um Code zum testen direkt im Atom auszuführen, da stellt sich der Proxy mal wieder in den Weg.

<!--more-->

{{< highlight bash "linenos=table,hl_lines=8 15-17,linenostart=1" >}}
apm config set https-proxy http://proxy.example.com:3128
apm config set http-proxy http://proxy.example.com:3128
{{< / highlight >}}

Atom wieder öffnen und Package installieren.
