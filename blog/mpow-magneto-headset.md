---
title: 'MPOW Magneto Headset'
author: "Rüdiger Küpper"
banner: "/img/cat/technik.jpg"
categories: ["Technik"]
date: Sat, 23 Jan 2016 00:03:31 +0000
draft: false
tags: [Alle Beiträge, Bluetooth, Musik, Technik, Telefon, Zubehör]
---

Die In-Ear Headphones Earpods wurden am 12 September 2012 von Apple vorgestellt. Wie üblich bei Apple wurden Zahlen genannt und dramatisch darüber berichtet wie schwer es ist einen solchen Kopfhörer zu entwickeln. Aber Apple rettet ja die Welt und hat drei Jahre Entwicklung in einen Kopfhörer gesteckt, um den Ultimativen Kopfhörer herzustellen. Apple hat dafür tausende von Ohrformen analysiert. So entstanden dann die Earpods, die fast jedem passen sollten.
<!--more-->

{{< postimage "" "earpods-2-300x300.jpg" "earpods-2-300x300.jpg" >}}


Mir passen diese Kopfhörer überhaupt nicht. Sie fallen nicht nur beim laufen alle paar Meter runter, nein selbst im stehen rutscht mir dieser Kopfhörer alle paar Minuten oder auch Sekunden aus dem Ohr. Ich benutze ein Headset um die Hände frei zu haben. Nicht um andauernd hingreifen zu müssen, damit es nicht komplett raus rutscht.

{{< postimage "" "earpods-1-300x225.jpg" "earpods-1-300x225.jpg" >}}


In letzer Zeit habe ich sie noch ein paar mal ausprobiert. Wir werden wohl nie Freunde werden. Also habe ich vor ein paar Tagen ein wenig nach einem neuen Headset gesucht. Schnell war klar, ein In-Ear wird es werden. Irgendwann viel mir das Headset von MPOW auf. Die Artikel und Videos über das MPOW Magneto waren durchweg sehr positiv. Dieses Headset ist ein Bluetooth Headset, hat kleine Bügel (Stabilisatoren) in verschiedenen Grössen zur weiteren Fixierung im Ohr.

{{< postimage "" "Mpow-Magneto-2-300x169.jpg" "Mpow-Magneto-2-300x169.jpg" >}}


Interessant ist die Magnetfunktion des Headsets. Sie hält nicht nur die beiden Seiten zusammen beim Tragen um den Hals, damit werden auch Anrufe angenommen/beendet oder Musik gestartet/gestoppt.

{{< postimage "" "Mpow-Magneto-3-300x288.jpg" "Mpow-Magneto-3-300x288.jpg" >}}


Wie üblich sind mehrere Ohrstöpsel in verschiedenen Grössen im Lieferumfang enthalten. Zusätzlich sorgen Stabilisatoren für einen besseren Halt im Ohr. Auch hier in unterschiedlichen Grössen. Die Kopfhörer sitzen ohne den Stabilisator sehr gut im Ohr, mit diesen ist aber noch ein weiterer Punkt im Ohr vorhanden der für halt sorgt. Sonst wird ein In-EarKopfhörer durch das Kabel meistens nach unten gezogen, sitzt dann schief im Ohr, was die Ursache für ein rausrutschen. Genau das verhindern die Stabilisatoren. In der ersten Zeit sind sie noch ungewohnt, aber man gewöhnt sich sehr schnell an sie und bemerkt sie nicht mehr.

{{< postimage "" "Mpow-Magneto-1-300x180.jpg" "Mpow-Magneto-1-300x180.jpg" >}}


Die Ohrstöpsel selbst sitzen sehr fest am Gehäuse. Beim austauschen für die passende Grösse musste man schon etwas ziehen. Das aufstecken der anderen war auch mit etwas fummeln verbunden. Sie werden aber wohl nie einfach abgehen und im Ohr stecken bleiben wenn man die Kopfhörer aus dem Ohr zieht. Das ist mir bei anderen In-Ears schon ein paar mal passiert. Bis jetzt wurde das Headset nur ein paar mal getestet. Die nächste Zeit wird zeigen wie es sich im Alltag bewährt. Da man sie einfach um den Nacken herumführen kann und bei nicht benutzen vorne durch die Magenete zusammen schnappen lassen kann, können sie auch nicht verloren gehen. Bei einem Anruf einfach auseinander ziehen und in die Ohren stecken. Wie erwähnt ist dies auch beim Musik hören benutzt werden. Wer bei der Arbeit Musik hört kennt die Unterbrechungen durch die Kollegen. Kopfhörer aus den Ohren ziehen und einfach zusammen schnappen lassen. Musik stoppt. Auseinanderziehen, in die Ohren stecken und die Musik spielt weiter. Neben den anderen Funktionen sind diese beiden für mich das Argument für den Kauf gewesen. Jetzt heisst es erst einmal testen.
