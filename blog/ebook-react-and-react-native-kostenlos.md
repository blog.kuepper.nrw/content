---
title: 'eBook React and React Native kostenlos'
date: Thu, 19 Jul 2018 09:42:20 +0000
author: "Rüdiger Küpper"
banner: "/img/cat/internet.jpg"
categories: ["Internet"]
draft: false
tags: [Application, Build React, E-Book, epub, Facebook's Relay, GraphQL technologies, Internet, Internet, platform, React, React Native, Routing, unified architecture, Web Application]
---

Bei [packtpub.com](https://www.packtpub.com/packt/offers/free-learning) gibt es wieder das tägliche kostenlose E-Book. "React and React Native" heißt das heutige Buch und könnte für einige interessant sein.

<!--more-->

> React and React Native Use React and React Native to build applications for desktop browsers, mobile browsers, and even as native mobile apps Build React and React Native applications using familiar component concepts Dive deep into each platform, from routing in React to creating native mobile applications that can run offline Use Facebook's Relay, React and GraphQL technologies, to create a unified architecture that powers both web and native applications
