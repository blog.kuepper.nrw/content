---
title: 'MPOW Bluetooth Headset Reset'
author: "Rüdiger Küpper"
date: Tue, 18 Dec 2018 18:42:05 +0000
draft: false
banner: "/img/cat/technik.jpg"
categories: ["Technik"]
tags: ["Bluetooth", "iPhone", "Musik", "Restore", "Technik", "Telefon", "Zubehör"]
---

Ich hatte 2016 ja [hier berichtet](https://blog.kuepper.nrw/2016/01/23/mpow-magneto-headset/): ich habe jetzt das MPOW Bluethoth Headset. Das benutze ich immer noch.

<!--more-->

{{< postimage "MPOW Bluetooth Headset Reset" "Mpow-Magneto-1.jpg" "Mpow-Magneto-1.jpg" >}}

Aus gegebenen Anlass mal ein Update zu dem Gerät. Funktioniert immer noch super und ich bin damit immer noch zufrieden. Vorgestern habe ich es einfach in den Rucksack geworfen und irgend etwas muss auf die Knöpfe gedrückt haben. Er blinkte nur noch wild. Gerade wollte ich einen Podcast hören und das Headset war nicht mehr gekoppelt. Mehrfach aus- und einschalten hat nicht geholfen. Wie Reset durchführen? Google wirft nur zig Anleitungen zum koppeln. Reset ist nirgendwo beschrieben. Ich hatte das Headset aber schon mal neu verbinden müssen. Aber erst einmal und das ist lange her und es war nur wegen neuem iDevice und nicht wegen einem Fehler. Es war etwas mit lange drücken. Also Power gedrückt, nichts. Die Taste für lauter ist es auch nicht. Es war die Leisetaste. Die einfach bei wild blinken 10 Sekunden gedrückt halten. Danach hört das blinken auf. Aber koppeln ging noch nicht. Dafür dann einfach ausschalten und wieder einschalten. Das Headset befindet sich dann im Pearingmodus. Am anderen Device taucht es dann auf, antippen und wird gekoppelt.
