---
title: 'Serverless Computing. What?'
author: "Rüdiger Küpper"
banner: "/img/cat/internet.jpg"
categories: ["Internet"]
date: Wed, 16 Aug 2017 11:17:54 +0000
draft: false
tags: [Cloud, Computing, Infrastructure, Internet, Internet, Linux, programming, Social Network]
---

René Büst erklärt in [seinem Artikel](https://www.computerwoche.de/a/serverless-infrastructure-erleichtert-die-cloud-nutzung,3314756) was "Serverless Infrastruktur" bzw. "Serverless Computing" ist.

<!--more-->

> Im Prinzip ist es nichts Besonderes, wenn Cloud-Anbieter ständig eine neue Sau durchs Dorf treiben. Die aktuelle Sau hört auf den Namen "Serverless Infrastructure" oder auch "Serverless Computing". Das Außergewöhnliche daran ist jedoch, dass sie einige Verwirrung stiftet. Ist nun das Ende der Server angebrochen? Bei weitem nicht - viel mehr verschiebt sich innerhalb des Cloud-Stacks mal wieder etwas weiter nach oben.
