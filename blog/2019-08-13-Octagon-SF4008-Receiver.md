---
title: "Octagon SF4008 Receiver"
author: "Rüdiger Küpper"
aliases:
  - /2019/08/13/octagon-sf4008-receiver/
  - /mediacenter.html
date: "Tue, 13 Aug 2019 15:16:10 GMT"
draft: false
banner: "/img/posts/2019/08/13/multimedia.png"
categories: ["Technik"]
tags: [Octagon,Satelit,Kabel,DVB-T,Receiver,DVB-S2,DVB-C,TV,]
toc: true
hoto: "/img/page-preview.jpg"
---

# Unser Octagon Receiver

{{< postimage "Octagon SF4008 Receiver" "sf4008.png" "sf4008.png" >}}


Wir haben seit über einem Jahr einen Ocatgon SF4008 Receiver an der Sat-Anlage.
Da ich immer wieder Aussagen oder Fragen höre wie:

* "Ach, das kann der Receiver?"
* "Wie, das machst Du auch damit?"
* uva.

werde ich mal versuchen zusammen zufassen wieso der Octagon Receiver ausgewählt wurde.

<!--more-->

Auswahlkriterien waren:

* Preis
* Sat und/oder Kabel und/oder DVB-T
* Aufnahmemöglichkeit auf Festplatte
* Alternativ oder parallel Aufnahme auf einem Netzwerklaufwerk
* Individuell anpassbar (installierte Funktionen/Oberfläche)
* Den HD+-Dreck brauchen wir nicht (wäre aber möglich)  
* Auch noch Updates wenn das Gerät älter ist

Die Liste wurde im Kopf noch viel länger was die gewünschten Funktionen waren.
Um das schon mal vorweg zu nehmen. Diese Liste im Kopf konnte der Ocatgon eigentlich komplett erfüllen.

# Preis und Zukunft

Der Octagon SF4008, andere Modelle oder auch von anderen Herstellern, die in diese Richtung gehen, sind auf dem ersten Blick für viele teuer. Aber ganz wichtig ist dabei zu beachten das es eben kein MediaMarkt 49,- € Receiver ist, sondern schon einiges mehr kann, besser gessagt zu einem Gerät gemacht werden kann was alles mögliche machen kann.

Wir setzen ihn aktuell nur für Sat TV ein. Sollten die Kabel-TV Anbieter endlich mal ihren Schrott besser machen könnten wir aber auch schnell um Kabel erweitern. Das wäre nur eine weitere Karte in den freien Slot stecken, Kabel rein und fertig.

Die aktuelle Karte im Slot 1 kann Dual Sat (DVB-S2), heisst zwei Kabel von der SChüssel bzw. der Weiche können einfach in das Gerät gesteckt werden und Bild-in-Bild oder Aufnahme und parallel gucken sind kein Problem.

Würden wir uns mal für DVB-T entscheiden könnten wir aber auch eine Karte für DVB-T reinstecken.

Vorteil: Egal was die nächsten Jahre passiert: Wir müssen kein neues Gerät kaufen und auch nicht viel neu einrichten, nur weil sich der Anschluß / die Quelle des TV Programms ändert.

# Unser Octagon ist immer noch ohne Festplatte

Es liegen zwar noch genug Festplatten herum die aus anderen Geräten noch übrig waren. Trotzdem hat der Octagon immer noch keine Festplatte eingebaut.
Der Octagon ist hier im Netzwerk per Kabel angeschlossen und hat auch Zugriff auf mehrere Freigaben auf einem Synology NAS.

Der Ocatagon kann z.B. auf ein Homeverzeichnis von einem User zugreifen, auf das ich auch von meinem Rechner schnell Zugriff habe und kann Daten für den Ocatgon dort ablegen oder Aufnahmen abrufen.

Aufnahmen kann man so auch auf den Rechner laden und sie unterwegs gucken. Oder wir kopieren Fotos und Videos auf das NAS, um sie anderen auf dem großen TV zu zeigen.

Ja, das könnten wir auch vom iPhone, iPad oder Rechner auch direkt über den AppleTV machen. Oder dem FireTV. Aber wir möchten das auch mal von unterwegs aus machen können. Oder wir lassen über Streaming ein oder mehrere Videos auf mehreren Geräten gleichzeitig abspielen. Zig Leute zu besuch und alle hängen um ein kleines Gerät und veruchen zu gucken? Nö, hier kann jeder einfach auf seinem Gerät mitgucken.

Das ganze funktioniert auch mit dem TV-Programm. Sogar von unterwegs. Möchte man etwas nicht unbedingt am Fernseher gucken, sondern einen Bericht/Doku im Sommer auf dem Balkon oder unterwegs Live gucken. Auch kein Problem, einfach streamen.
Das geht auch parellel zum TV gucken. Brauchen wir auch nicht so oft, aber immer wenn wir mal was im TV gucken und jemand aus der Family kurz nach oben kommt, schellt und mal eben was durch den Dokumenten-Scanner schicken will, weil er es per Mail versenden möchte wird es gerne benutzt. iPhone oder iPad in die Hand, Streaming des aktuellen Senders auf das iPad und so nichts verpassen. Die anderen können ja auch ganz normal am TV weitergucken.

# Anpassen wie es einem gefällt

Beim anpassen von Funktionen, Oberfläche oder gar Senderlisten ist alles mögliche machbar. Alleine schon das Senderlisten anpassen war wichtig. Wer schon mal Unitymedia / KabelBW hatte und da alle paar Monate eine neue Senderliste untergejubelt bekommen hat, der wird wissen wie es dazu kommt das man für den Anbieter nur noch hass empfinden kann. Stundenlanges hin- und herschieben von Sendern mit einer sehr schlechten Bedieneroberfläche.

Beim Octagon kann man das über eine viel bessere Oberfläche viel schneller erledigen.
Oder man lädt sich die Aktuelleliste per Sat oder Kabel, je nach Anschluss vom Anbieter. Dann lädt man eine Datei vom Ocatgon herunter. Diese Datei öffnet man in einem Texteditor. Schiebt alles so zurecht wie man die Sender haben möchte und kopiert die Datei wieder auf den Ocatgon. Die Datei wird eingelesen und schon ist die Senderliste wie gewünscht.

Wer jetzt nicht unbedingt immer neue Sender unbedingt sofort haben muss, der kann sich ein paat Zeilen Script schreiben, welche die gewünscht Senderliste aus einer neuen Anbieterliste zusammenbaut und alle restlichen neuen Sender einfach ans Ende packt. Die kann man dann auch schnell über das Webinterface verschieben.

Eine andere Möglichkeit ist eine Liste von jemanden anderem zu benutzen. Das mache ich bei unserem Ocatgon so. Ich habe die Liste seit der Ersteinrichtung nicht mehr angepackt. Der hat eine Webadresse zu einer Liste bekommen und überprüft regelmässig ob sich etwas an den Sendern verändert hat.
Die neue Liste wird dann heruntergeladen und installiert.

Da kümmert sich jemand anderes drum und der macht es richtig gut. Wir haben noch nie mitbekommen das ein Sender weg ist weil auf einem Transponder sich eteas geändert hat. Da müsste ich mal nachgucken wer das ist, mich mal bedanken und ggf. mal mit einer Kleinigkeit erkenntlich zeigen. Der Typ macht das als Hobby und in einer Qualität, Top Arbeit.

Auch die Oberfläche ist anpassbar und erweiterbar bis ins kleinste Detail. Es gibt zig vorgefertigte Designs, die per Paketverwaltung einfach installiert werden. Aktivieren und gucken ob es gefällt. Man kann aber auch einfach noch ein paar andere installieren und zwischen den Oberflächen wechseln. Gerade am Anfang sehr nett um zusehen was man eigentlich wie haben will.

# Bildergalerie von der Oberfläche

{{< foldergallery src="images/galleries/2019/08/Octagon-SF4008-Receiver" >}}

Für die Bilder habe ich einfach ein paar Sender, Menüs und Plugins aufgerufen und dann am Rechner einen einfachen Befehl ausgeführt.

```
curl -s -u Benutzer:Passwort "http://10.0.2.105/grab?format=png&r=1280&&filename=screen1.png" > screen-$(date "+%H-%M-%S").png
```

Das erstellt mir einfach einen Screenshot vom Bild was der Octagon in dem Moment auf dem TV anzeigt. Die Bilder werden mit dem Name `screen-<Stunde>-<Minute>-<Sekunde>.png` gespeichert. Dabei sind `r=1280` sind 720p und `r=1920` wären 1080p.

# PlugIns für alles

Es gibt zum Beispiel:

* Mediacenter Fotos/Videos von der Festplatte oder Netzwerklaufwerk abspielen
* Filebrowser
* IMDB für Infos zu Filmen/Serien
* Moviecutter (Videos/Aufnahmen schneiden)
* OpenWebIf (Webinterface um per Browser auf den Ocatgon zugreifen zu können)
* Youtube
* ZDF-Mediathek

Die Liste ist sehr lang. Komplett verfügbar sind über 4000 Pakete. Davon sind natürlich einige Linux System Tools und Librarys. Aktuell haben ich insgesamt 534 installiert. WakeOnLan, Fritzbox, Speedtest uvm.

Fritzbox ist dafür installiert um Anrufe auf dem Festnetz mit einer kleinen Infobox kurz auf dem TV anzuzeigen. Kein störendes klingeln mehr wenn man eigentlich nur TV gucken will.

# Wir steuern auch per Sprache

Da wir auch das Webinterface installiert haben können wir per Api jeden Befehl der Fernbedienung auch von einem Rechner aus ausführen. Durch eine Einstellung wurde der Untertitel immer angezeigt. Welche Taste das ausschaltet haben wir immer vergessen. Irgend wann habe ich einfach die Videotext-Taste so belegt das der Untertitle an- bzw ausgeschaltet wurde. Das hat Linda dann aber auch immer wieder vergessen. Also habe ich die Api vom Webinterface dazu benutzt mit HTTP Requests zusammen zu bauen die das einfach über die API machen.
Die HTTP Aufrufe habe ich in HomeBridge geworfen und jetzt kann man einfach den Untertitle einfach per Sprache ausschalten:

> Alexa, schalte Untertitel aus.

Das kann der Octagon von sich aus so nicht. Dazu ist HomeBridge und Amazon Alexa oder AppleTV mit Siri nötig. Beim schrieben denke ich gerade:

> Ich will das nicht so zu 100% sagen, weil irgendwo im Netz hat jemand das bestimmt auch schon mit dem Octagon genau so gemacht.`

Da hier ein HomeBridge und FHEM Server für unser SmartHome vorhanden ist macht der das einfach für uns so möglich.

# Der Octagon macht vieles einfach entspannter

* Sat, Kabel, DVB-T auswechselbar oder kombinierbar.
* Viele Möglichkeiten zum Erweitern von Funktionen
* Er wird noch sehr lange Updates bekommen.

# Was der Octagon hier nie machen wird

Wir werden den Octagon niemals mit einer HD+ Karte betreiben. ARD/ZDF sind eh alle HD und die privaten sind es nicht wert da Geld für mehr Pixeln einzuwerfen.
Ich sage da immer:

> Hätte man bei der Einführung von Farbfernsehen damals Geld verlangt wäre die Entwicklung stehen geblieben und wir hätten heute noch Schwarz/Weiß`

Ausserdem kann ein Sender sehr viel einschränken wenn er möchte. Er kann jeder Zeit sagen das Aufnahmen nicht mehr oder für bestimmte Sendungen/Filme nicht mehr erkaubt ist. Oder Aufnahmen sind erlaubt, aber man darf nicht mehr vorspulen. Das sind einfach zu viele unberechenbare Einschränkungen, die einfach nur unverschämt sind.

# Updates der Firmware und Oberfläche

Bei den üblichen SAT-/Kabel-/DVB-T-Receivern hat man immer das Problem das sie nach ein paar Jahren kein Update mehr bekommen. Ausserdem ist das Update bei den meisten Geräten immer fast unmöglich gewesen. Die Boxen haben zwar USB, serielle Schnittstelle, aber wer hat heute noch die passenden Geräte um damit ein binary des Updates auf das Gerät zu schieben?
Viele der Gerate haben zwar noch funktioniert, wurden aber in die Tonne geschmissen weil Änderungen in der Infrastruktur nicht auf den Geräten angekommem sind. Da sind Anbieter und Hersteller eigentlich in der Pflicht.

Umweltschutz interessiert da wohl keinen. Und so wurden jahrelang Millionen von Geräten sinnlos in den Müll geworfen.

Der Octagon hat Netzwerk per Kabel. Könnte aber auch per USB WiFi Adapter per WLAN ins heimische Netzwerk gebracht werden. Andere Modelle haben sogar WLAN direkt dabei.

# Octagon mit WLAN, da geht noch mehr ;-)

Wer zuhause bescheidenes WLAN hat, da der Anschluss mal wieder im Flur oder gar in einer anderen Etage ist. Einfach mal dran denken: Auf dem Octagon läuft ein Linux.

Wenn der Octagon mit WiFi per Kabel ins Netzwerk gebracht werden kann:

* Octagon per Kabel ins Netzwerk  
* WiFi vom Octagon als AccessPoint konfigurieren
* Weiterleitung zwischen WLAN und Kabel aktivieren
* Andere Geräte können sich per Wifi jetzt auch mit dem Ocatgon verbinden und kommen dann über den ins Netz.

So viel zu den Möglichkeiten die so ein Gerät mitbringt.
