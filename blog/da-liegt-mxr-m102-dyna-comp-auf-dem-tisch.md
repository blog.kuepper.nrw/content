---
title: 'Da liegt MXR M102 "dyna comp" auf dem Tisch'
author: "Rüdiger Küpper"
banner: "/img/cat/musik.png"
categories: ["Musik"]
date: Mon, 01 Feb 2016 15:24:59 +0000
draft: false
tags: [Alle Beiträge, Effektgeräte, Guitar, Musik]
---

Vor ein paar Wochen habe ich für einen Arbeitskollegen einen Bass mit nach Essen genommen. Fahre ja eh jedes Wochenende nach Essen. Da bietet sich das an für jemanden etwas mit zunehmen. Der Käufer hat sich für seinen Bass entschieden und ich habe das Gigbag wieder mit zurück nach Karlsruhe genommen. Gehe in sein Büro um es wieder abzuliefern. Liegt da eine kleine schwarze Box mit rotem Aufkleber.
<!--more-->

{{< postimage "MXR M102 Dyna Comp" "IMG_9117-e1454339397491-1024x768.jpg" "IMG_9117-e1454339397491-1024x768.jpg" >}}

MXR M102 "Dyna Comp" steht drauf. Er hat sich den bestellt und mit seinem Bass getestet. Ihm klaut der Dyna zu viele Bässe von seinem Bass. Ein anderer Kollege hat das Teil an seiner Stratocaster getestet und fand es dafür genau richtig. Das Angebot den "Dyna Comp" auch mal anzuspielen konnte ich natürlich nicht ausschlagen. Also die Box unter dem Arm geklempt und nachher dann mal antesten.

{{< postimage "MXR M102 dyna comp" "IMG_7260-e1454339420251-225x300.jpg" "IMG_7260-e1454339420251-768x1024.jpg" >}}

Soundbeispiele werden folgen. Da ich dieses Wochenende mal das Zoom mit Audiointerface in Essen gelassen habe, kann ich auch gleich Aufnahmen mit dem Samson C-03 U Studio Großmembran Kondensator Mikrofon testen. Dank integrierten USB Audiointerface komme ich damit auch ohne Probleme für die Aufnahmen in den Rechner.
