---
title: 'Kommentarregeln'
author: "Rüdiger Küpper"
date: "2019-08-05T12:05:53.274Z"
draft: false
---

## Regeln für die Kommentare

Jeder soll zu unseren Themen seine Meinung abgeben können. Das muss sich aber auf einem vernünftigen Niveau bewegen und darf niemanden beleidigen oder sonstwie ernsthaft beeinträchtigen. Dafür sind ein paar Regeln unabdingbar - wenn sich alle daran halten, steht einem regen Meinungsaustausch nichts mehr im Wege.

Die folgenden fünf Regeln gelten für alle Nutzer der Kommentarfunktion und führen bei Nichtbeachten zu einer Verwarnung und im Wiederholungsfall zu einer Sperrung des Nutzers.

### 1. Allgemeines

Die Kommentare dienen dem allgemeinen Meinungsaustausch und der Diskussion zu aktuellen Themen unseres Portals. Die Gesetze der Bundesrepublik Deutschland sind zu beachten. Missachtung kann strafrechtliche Verfolgung nach sich ziehen.

Der Zugang zu den Kommentaren und deren Nutzung kann nicht in irgendeiner Form verlangt, erklagt, oder anderweitig erzwungen werden. Der Betreiber behält sich das Recht vor, jederzeit ohne Angabe von Gründen einzelnen Nutzern das Privileg der Nutzung temporär oder permanent zu entziehen. Der Nutzer erkennt an, dass als Autor seiner Nutzerkommentare der Vor- und Nachname angezeigt werden, die er als Klarname in seinem Mailaccount verwendet.



### 2. Verbote

In den Kommentaren soll eine für die Allgemeinheit interessante und rechtskonforme Diskussion stattfinden. Ausdrücklich verboten sind unter anderem:

Rassismus und Gewaltpropaganda
Aufruf zu Straftaten
Diskriminierende oder menschenverachtende Äußerungen
Beleidigungen, Verleumdungen, Schmähkritik, Hetze, Hassrede
Verletzungen des Persönlichkeitsrechts und der Anonymität Dritter
Verletzungen des Urheberrechts
Werbung in jeder Art oder Spam
Links zu externen Webseiten
Diskussionsferne Beiträge bzw. Kommentare, die nichts mit dem Diskussionsthema zu tun haben
Kommentare, die in einer für die meisten Nutzer unverständlichen Sprache oder Schrift verfasst sind
Die Moderatoren und Administratoren werden Beiträge, die gegen die Regeln verstoßen, sofort nach Kenntnisnahme löschen. Weiterhin muss der Verursacher mit einem Bann von der Kommentarfunktion sowie gegebenenfalls mit strafrechtlichen Konsequenzen rechnen.



### 3. Verantwortlichkeit

Jeder Nutzer ist für die von ihm erstellen Beiträge und deren Inhalte ausschließlich selbst verantwortlich. Es gilt das Recht auf freie Meinungsäußerung, wobei Verstöße gegen die unter Punkt 2 genannten Verbote nicht darunter fallen. Der Betreiber übernimmt keine Verantwortung für die Beiträge der Nutzer.



### 4. Datensammlung

Es ist untersagt, die Daten der Nutzer unserer Kommentarfunktion zu sammeln und für eigene Zwecke zu verwenden. Auch dürfen keine Beiträge mit dem Zweck der Sammlung persönlicher Daten angelegt werden.

Geben Sie als Nutzer der Kommentarfunktion auf keinen Fall Ihr Passwort bekannt. Auch wenn Sie in Kontakt mit einem Moderator oder Administrator stehen, wird dieser nie nach Ihrem persönlichen Passwort fragen.


### 5. Form der Beiträge

Vermeiden Sie komplette Wörter in Großbuchstaben, speziell in Überschriften. Auch auf die mehrfache Verwendung von Satzzeichen (z.B. !!!) sollten Sie verzichten. Benutzen Sie Zitate sparsam und zitieren Sie nur Teile anderer Beiträge, auf die sich Ihre Antwort bezieht. Dies alles dient einer besseren Lesbarkeit und der Übersichtlichkeit und hilft Ihnen und anderen Nutzern, sich besser und schneller in den Kommentaren zurechtzufinden.
