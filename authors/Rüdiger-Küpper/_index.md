---
name: Rüdiger Küpper
photo: 'https://blog.kuepper.nrw/images/galleries/2019//thumbs/DSC04881.jpg'
twitter: "@ruedigerp"
---
Rüdiger Küpper (March 20, 1977) Linux Geek aus Leidenschaft.

### Über mich

Das hier werde ich mal bei Zeiten mit mehr Infos über mich füllen.

### Kontakt

> Rüdiger Küpper

> Frintroper Strasse 371

> 45359 Essen

> Telefon: +49.20145312743

> Fax: +49.20145312745

> E-Mail: [ruediger+acc.blogs.impressum@pretzlaff.info](mailto:ruediger+acc.blogs.impressum@pretzlaff.info)

> GPGKey: [0x3A9E833B2F49E4EF](http://keys.gnupg.net/pks/lookup?op=get&search=0x3A9E833B2F49E4EF)
