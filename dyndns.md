+++
title = "DynDns"
id = "page"
+++

## Mehr Flexibilität durch free dynamic DNS

DynDNS bietet sich vor allem dann an, wenn Sie einen lokalen Rechner wie einen Heimcomputer oder Firmenserver dauerhaft über das Internet zugänglich machen möchten.
In der Regel ist ein solcher Rechner über die dynamische IP-Adresse des Internetdienstleisters an das Internet angebunden. Bei dieser Art der Adressierung wird dem Rechner bei jeder neuen Verbindung mit dem Internet automatisch eine neue IP-Adresse zugewiesen. Ein Zugriff durch Nutzer von außen ist daher nur dann möglich, wenn die aktuelle IP-Adresse des Rechners bekannt ist.

#### Diese Problematik umgehen Sie bequem mit dem free dynamic DNS Service.

Mit DynDNS (Dynamic Domain-Name-Service) profitieren Sie bei zahlreichen Hosting-Paketen von einem Internetdienst, der Ihnen die Möglichkeit bietet, einer sich ändernden IP-Adresse einen festen Domainnamen zuzuordnen. Dies hat zahlreiche Vorteile für Sie. Wenn Sie beispielsweise von unterwegs auf Ihren eigenen Rechner zugreifen oder auf Ihrem lokalen Rechner Daten zum Download via FTP über das Internet anbieten möchten, nutzen Sie DynDNS, um eine Ihrer bei registrierten Domains oder Subdomains auf den entsprechenden Rechner umzuleiten.

So machen Sie auf dem lokalen Rechner betriebene Internetdienste leichter zugänglich, stellen Streaming-Media bereit oder betreiben einen eigenen Mail-Server auf dem lokalen Firmenrechner. Dank der dynamischen Aktualisierung durch DynDNS bleibt Ihr Rechner für Ihre Nutzer auch dann erreichbar, wenn diese die aktuelle IP-Adresse nicht kennen.

{{< foldergallery src="images/galleries/2019/" >}}

T&V

{{< foldergallery src="images/galleries/2019/08/Taddl-Volker/" >}}
