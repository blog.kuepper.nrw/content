+++
title = "VPN"
id = "page"
+++

## 1. VPN für den Schutz Ihrer Privatsphäre

Eines der wichtigsten Argument für die Nutzung VPN, ist der Schutz der eigenen Privatsphäre bei der Internetnutzung. Anbieter wie HideMyAss, IPVanish oder Private Internet Access bieten ein Virtual Private Network an, mit dem Sie eine andere IP-Adresse erhalten und Ihre Daten durch VPN-Verschlüsselung vor Zugriff von außen geschützt sind. Auf diesem Weg sind Sie effektiv vor Hackern und Datendieben geschützt.

## 2. VPN schützt in öffentlichen WLAN-Netzen

Wer denkt, dass WLAN-Netzen sicher vor Angriffen über das Internet sind, der irrt. Ganzt besonders in öffentlichen WLAN Netzen, wie z.B. in Hotels, Cafes und Restaurants können jederzeit gefahren lauern. Diese Hotspots verfügen häufig +ber keinerlei Sicherheitsmaßnahmen. Darüber hinaus können andere WLAN-Nutzer in dem Hotspot andere Rechner angreifen und auf Datenspionage gehen. Mit einem VPN surfen Sie im WLAN-Hotspot über einen VPN-Tunnel nach außen abgeschirmt.

